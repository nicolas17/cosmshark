// SPDX-FileCopyrightText: 2022 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#define WS_BUILD_DLL

#include <epan/packet.h>
#include <epan/proto.h>
#include <epan/dissectors/packet-tcp.h>
#include <ws_attributes.h>
#include <ws_symbol_export.h>
#include <ws_version.h>

#ifndef VERSION
#define VERSION "0.0.0"
#endif

WS_DLL_PUBLIC_DEF const gchar plugin_version[] = VERSION;
WS_DLL_PUBLIC_DEF const int plugin_want_major = WIRESHARK_VERSION_MAJOR;
WS_DLL_PUBLIC_DEF const int plugin_want_minor = WIRESHARK_VERSION_MINOR;

WS_DLL_PUBLIC void plugin_register(void);

static int proto_hypercosm = -1;
static dissector_handle_t handle_hypercosm;

static gint ett_hypercosm = -1;
static int hf_hypercosm_session_uuid = -1;
static int hf_hypercosm_compression_type = -1;
static int hf_hypercosm_wire_length = -1;
static int hf_hypercosm_response_in = -1;
static int hf_hypercosm_response_to = -1;
static int hf_hypercosm_sequence_number = -1;
static int hf_hypercosm_command_number = -1;
static int hf_hypercosm_target_object = -1;
static int hf_hypercosm_status_code = -1;

static const value_string compression_types[] = {
    { 0, "None" },
    { 0, NULL }
};

static const value_string status_codes[] = {
    { 0,  "Success" },
    { -1, "Object or function not found" },
    { -2, "Deserialization failed" },
    { 0, NULL }
};

typedef struct {
    // which side is the client
    address client_addr;
    port_type client_ptype;
    guint client_port;

    // packet number where we saw the handshake UUIDs
    guint32 client_uuid_packet;
    guint32 server_uuid_packet;

    wmem_map_t *pdus;
} hypercosm_conv_t;

typedef struct {
    guint32 call_frame;
    guint32 response_frame;
} hypercosm_transaction_t;

static hypercosm_conv_t* find_or_create_hypercosm_conv(packet_info *pinfo)
{
    conversation_t *conversation = find_or_create_conversation(pinfo);

    hypercosm_conv_t *conv_data = (hypercosm_conv_t*)conversation_get_proto_data(conversation, proto_hypercosm);
    if (!conv_data) {
        conv_data = wmem_new0(wmem_file_scope(), hypercosm_conv_t);
        conv_data->pdus = wmem_map_new(wmem_file_scope(), g_direct_hash, g_direct_equal);

        conversation_add_proto_data(conversation, proto_hypercosm, (void*)conv_data);
    }
    return conv_data;
}

static gboolean packet_is_from_client(hypercosm_conv_t *conv_data, packet_info *pinfo)
{
    return (conv_data->client_ptype == pinfo->ptype &&
            conv_data->client_port == pinfo->srcport &&
            addresses_equal(&conv_data->client_addr, &pinfo->src));
}

static void tree_add_uleb128(proto_tree *tree, int hfindex, tvbuff_t *tvb,
    guint *poffset, guint max_bits, guint *retval)
{
    guint offset = *poffset;

    guint index = 0;
    guint result = 0;
    guint shift = 0;
    while (shift < max_bits) {
        guint8 this_byte = tvb_get_guint8(tvb, offset+index);
        index++;
        result |= (this_byte & 0x7f) << shift;
        if ((this_byte & 0x80) == 0) break;
        shift += 7;
    }
    if (max_bits > 32) {
        proto_tree_add_uint64(tree, hfindex, tvb, offset, index, result);
    } else {
        proto_tree_add_uint(tree, hfindex, tvb, offset, index, result);
    }
    *poffset = offset+index;

    if (retval) {
        *retval = result;
    }
}

static int
dissect_hypercosm_packet(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data _U_)
{
    proto_item *ti = proto_tree_add_item(tree, proto_hypercosm, tvb, 0, -1, ENC_NA);
    proto_tree *hypercosm_tree = proto_item_add_subtree(ti, ett_hypercosm);

    hypercosm_conv_t* conv_data = find_or_create_hypercosm_conv(pinfo);
    if (conv_data->client_ptype == PT_NONE) {
        // First packet we get, assume it's the client and record it for posterity.
        // This may not be the best idea, maybe I can dig into the TLS conversation data
        // and find its idea of who's the client (which is based on ClientHello).
        copy_address_wmem(wmem_file_scope(), &conv_data->client_addr, &pinfo->src);
        conv_data->client_ptype = pinfo->ptype;
        conv_data->client_port = pinfo->srcport;
    }
    gboolean is_from_client = packet_is_from_client(conv_data, pinfo);

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "Hypercosm");

    if (is_from_client && (conv_data->client_uuid_packet == 0 || conv_data->client_uuid_packet == pinfo->num)) {
        col_append_sep_fstr(pinfo->cinfo, COL_INFO, ", ", "Client handshake");
        proto_tree_add_item(hypercosm_tree, hf_hypercosm_session_uuid, tvb, 0, 16, ENC_BIG_ENDIAN);
        conv_data->client_uuid_packet = pinfo->num;
    } else if (!is_from_client && (conv_data->server_uuid_packet == 0 || conv_data->server_uuid_packet == pinfo->num)) {
        col_append_sep_fstr(pinfo->cinfo, COL_INFO, ", ", "Server handshake");
        proto_tree_add_item(hypercosm_tree, hf_hypercosm_session_uuid, tvb, 0, 16, ENC_BIG_ENDIAN);
        conv_data->server_uuid_packet = pinfo->num;

    } else {
        guint offset = 0;
        proto_tree_add_item(hypercosm_tree, hf_hypercosm_compression_type,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
        offset++;
        tree_add_uleb128(hypercosm_tree, hf_hypercosm_wire_length, tvb, &offset, 64, NULL);

        guint sequence_number = 0;
        tree_add_uleb128(hypercosm_tree, hf_hypercosm_sequence_number, tvb, &offset, 64, &sequence_number);

        gboolean is_request = ((sequence_number & 1) == 0);
        // call_number is the same for a call and its corresponding response,
        // but different for a call from the client and a call from the server.
        // This is just for convenience in the dissector, not a protocol concept.
        // It's used as the key in the pdus hash table.
        guint call_num = sequence_number ^ (is_from_client?1:0);

        hypercosm_transaction_t* call_info;
        if (!PINFO_FD_VISITED(pinfo)) {
            if (is_request) {
                call_info = wmem_new0(wmem_file_scope(), hypercosm_transaction_t);
                call_info->call_frame = pinfo->num;
                call_info->response_frame = 0;
                wmem_map_insert(conv_data->pdus, GUINT_TO_POINTER(call_num), (void*)call_info);
            } else {
                call_info = wmem_map_lookup(conv_data->pdus, GUINT_TO_POINTER(call_num));
                if (call_info) {
                    call_info->response_frame = pinfo->num;
                }
            }
        } else {
            call_info = wmem_map_lookup(conv_data->pdus, GUINT_TO_POINTER(call_num));
        }

        if (is_request) {
            col_append_sep_fstr(pinfo->cinfo, COL_INFO, ", ", "Call");

            if (call_info && call_info->response_frame) {
                proto_item* it = proto_tree_add_uint(hypercosm_tree, hf_hypercosm_response_in, tvb, 0, 0, call_info->response_frame);
                proto_item_set_generated(it);
            }

            tree_add_uleb128(hypercosm_tree, hf_hypercosm_command_number, tvb, &offset, 32, NULL);
            tree_add_uleb128(hypercosm_tree, hf_hypercosm_target_object, tvb, &offset, 64, NULL);
        } else {
            col_append_sep_fstr(pinfo->cinfo, COL_INFO, ", ", "Response");

            if (call_info && call_info->call_frame) {
                proto_item* it = proto_tree_add_uint(hypercosm_tree, hf_hypercosm_response_to, tvb, 0, 0, call_info->call_frame);
                proto_item_set_generated(it);
            }

            proto_tree_add_item(hypercosm_tree, hf_hypercosm_status_code, tvb, offset, 1, ENC_LITTLE_ENDIAN);
            offset++;
        }
    }
    col_set_fence(pinfo->cinfo, COL_INFO);

    return tvb_captured_length(tvb);
}

static guint
get_hypercosm_command_len(packet_info *pinfo, tvbuff_t *tvb, int offset, void *data _U_)
{
    hypercosm_conv_t* conv_data = find_or_create_hypercosm_conv(pinfo);
    if (conv_data->client_ptype == PT_NONE || conv_data->client_uuid_packet == pinfo->num) {
        // We didn't see any data yet, so this must be the client's UUID
        // (or we already *know* this is the client UUID because we parsed it before)
        return 16u;
    } else if (!packet_is_from_client(conv_data, pinfo) && (conv_data->server_uuid_packet == 0 || conv_data->server_uuid_packet == pinfo->num)) {
        // If this packet is from the server and we didn't get the server UUID yet, this must be it
        // (or we already *know* this is the server UUID because we parsed it before)
        return 16u;
    } else {
        DISSECTOR_ASSERT(conv_data->client_uuid_packet > 0 && conv_data->server_uuid_packet > 0);

        offset += 1;

        guint index = 0;
        guint result = 0;
        guint shift = 0;
        while (shift < 32) {
            // we're only guaranteed to have 2 bytes of data in the tvb
            // so we need to check if there is another byte available before reading it
            if (!tvb_offset_exists(tvb, offset+index)) {
                // not enough bytes in the packet to parse the length
                return 0;
            }
            guint8 this_byte = tvb_get_guint8(tvb, offset+index);
            index++;
            result |= (this_byte & 0x7f) << shift;
            if ((this_byte & 0x80) == 0) break;
            shift += 7;
        }

        // +1 for the compression type before the length
        return result+index+1;
    }
}

static int
dissect_hypercosm(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
    tcp_dissect_pdus(tvb, pinfo, tree, TRUE, 2,
                     get_hypercosm_command_len, dissect_hypercosm_packet, data);
    return tvb_captured_length(tvb);
}

static void
proto_register_hypercosm(void)
{
    static hf_register_info hf[] = {
        { &hf_hypercosm_session_uuid,
            { "Session UUID", "hypercosm.session_uuid",
            FT_GUID, BASE_NONE,
            NULL, 0x0, NULL, HFILL }
        },
        { &hf_hypercosm_compression_type,
            { "Compression type", "hypercosm.compression",
            FT_UINT8, BASE_DEC,
            VALS(compression_types), 0x0, NULL, HFILL }
        },
        { &hf_hypercosm_wire_length,
            { "Packet length", "hypercosm.wire_length",
            FT_UINT64, BASE_DEC,
            NULL, 0x0, NULL, HFILL }
        },
        { &hf_hypercosm_sequence_number,
            { "Sequence number", "hypercosm.sequence_number",
            FT_UINT64, BASE_DEC,
            NULL, 0x0, NULL, HFILL }
        },
        { &hf_hypercosm_response_in,
            { "Response In", "hypercosm.response_in",
            FT_FRAMENUM, BASE_NONE,
            FRAMENUM_TYPE(FT_FRAMENUM_RESPONSE), 0x0,
            "The response to this call is in this frame", HFILL }
        },
        { &hf_hypercosm_response_to,
            { "Request In", "hypercosm.response_to",
            FT_FRAMENUM, BASE_NONE,
            FRAMENUM_TYPE(FT_FRAMENUM_REQUEST), 0x0,
            "This is a response to the call in this frame", HFILL }
        },
        { &hf_hypercosm_command_number,
            { "Command number", "hypercosm.command_number",
            FT_UINT32, BASE_DEC,
            NULL, 0x0, NULL, HFILL }
        },
        { &hf_hypercosm_target_object,
            { "Target object", "hypercosm.target_object",
            FT_UINT64, BASE_DEC,
            NULL, 0x0, NULL, HFILL }
        },
        { &hf_hypercosm_status_code,
            { "Status code", "hypercosm.status_code",
            FT_INT32, BASE_DEC,
            VALS(status_codes), 0x0, NULL, HFILL }
        }
    };
    static gint *ett[] = {
        &ett_hypercosm
    };
    proto_hypercosm = proto_register_protocol(
        "Hypercosm",
        "Hypercosm",
        "hypercosm"
    );
    proto_register_field_array(proto_hypercosm, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
}

static void
proto_reg_handoff_hypercosm(void)
{
    handle_hypercosm = create_dissector_handle(dissect_hypercosm, proto_hypercosm);
    dissector_add_uint("tls.port", 12345, handle_hypercosm);
}

void
plugin_register(void)
{
    static proto_plugin plug;

    plug.register_protoinfo = proto_register_hypercosm;
    plug.register_handoff = proto_reg_handoff_hypercosm;
    proto_register_plugin(&plug);
}
